import React from 'react';
import { 
    Route, 
    NavLink, 
    BrowserRouter as Router, 
    Switch 
} from 'react-router-dom';
import Notfound from './notfound';
import './header.css';
import Home from './home';
import Users from './users';
import Contact from './contact';
import Registration from './registration';

export class Header extends React.Component {
    render() {
        return (
            <div>
                <Router>
                    <header className='header'>
                        <ul className='shako1'>
                            <li className='shako'>
                                <NavLink exact activeClassName='active' to="/home">Home</NavLink>
                            </li>
                            <li className='shako'>
                                <NavLink activeClassName='active' to="/users">Users</NavLink>
                            </li>
                            <li className='shako'>
                                <NavLink activeClassName='active' to="/contact">Contact</NavLink>
                            </li>
                            <li className='shako'>
                                <NavLink activeClassName='active' to="/registration">Registration</NavLink>
                            </li>
                        </ul>
                        <input type="text" />
                        <button>Search</button>
                    </header>
                    <div className='main'> 
                        <Switch>
                            <Route exact path="/" />
                            <Route path="/home" component={Home} />
                            <Route path="/users" component={Users} />
                            <Route path="/contact" component={Contact} />
                            <Route path="/registration" component={Registration} />
                            <Route component={Notfound} />
                        </Switch> 
                    </div>
                </Router>
            </div>
        )
    }
}