import React from 'react';
import axios from 'axios';

class Books extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            repos: []
        }
    }
    componentDidMount() {
        axios.get("https://www.anapioficeandfire.com/api/books")
            .then(res => {
                const gotApi = res.data;
                this.setState({
                    repos: gotApi
                })
            })
    }

    render() {
        return (
            <React.Fragment>
                <table>
                    <thead>
                        <tr>
                            <td>Book Name</td>
                            <td>Number Of Pages</td>
                            <td>Publisher:</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.repos.map(repo => (
                            <tr>
                                <td>
                                    {repo.name}
                                </td>
                                <td>
                                    {repo.numberOfPages}
                                </td>
                                <td>
                                    {repo.publisher}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
export default Books