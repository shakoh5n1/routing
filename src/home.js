import React from 'react';
import { 
    Route, 
    NavLink, 
    BrowserRouter as Router, 
    Switch 
} from 'react-router-dom';
import Books from './books';
import Characters from './characters';
import Houses from './houses';
// import Notfound from './notfound';

class Home extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <h1>Game of Thrones</h1>
                    <NavLink to='/home/books'>Books</NavLink>
                    <NavLink to='/home/characters'>Characters</NavLink>
                    <NavLink to='/home/houses'>Houses</NavLink>
                </div>
                <Switch>
                    <Route exact path='/home/books' component={Books} />
                    <Route path="/home/characters" component={Characters} />
                    <Route path="/home/houses" component={Houses} />
                    {/* <Route component={Notfound} /> */}
                </Switch>
            </Router>
        )
    }
}

export default Home
