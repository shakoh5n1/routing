import React from 'react';
import axios from 'axios';

class Characters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            repos: [],
            url: "https://www.anapioficeandfire.com/api/characters",
            page: "?page=",
            number: 0
        }
    }

    

    componentDidMount() {
        axios.get(this.state.url)
            .then(res => {
                const gotApi = res.data;
                this.setState({
                    repos: gotApi
                })
            })
    }

    hendleClick = () => {
        this.setState({
            url : `${this.state.url}${this.state.page}${this.increment} `
        })
        
    }

    increment = () => {
        this.setState({
            number: this.state.number+1
        })
    }

    render() {
        return (
            <React.Fragment>
                <table>
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Culture</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.repos.map(repo => (
                            <tr>
                                <td>
                                    {repo.name}
                                </td>
                                <td>
                                    {repo.gender}
                                </td>
                                <td>
                                    {repo.culture}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button onClick={this.increment}>Next Page</button>
            </React.Fragment>
        )
    }
}
export default Characters

