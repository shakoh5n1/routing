import React from 'react';
import { Route, Link } from 'react-router-dom';

const User = ({ match }) => <p>{match.params.id}</p>

class Users extends React.Component {
    render() {
        console.log(this.props)
        // const { url } = this.props.match
        return (
            <div>
                <h1>Users</h1>
                <strong>select a user</strong>
                <ul>
                    <li>
                        <Link to="/users/books">Books</Link>
                    </li>
                    <li>
                        <Link to="/users/characters">Characters</Link>
                    </li>
                    <li>
                        <Link to="/users/houses">Houses</Link>
                    </li>
                </ul>
                <Route path='/users/:id' component={User} />
            </div>
        )
    }
}

export default Users