import React from 'react';
import axios from 'axios';

class Registration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            repos: []
        }
    }
    componentDidMount() {
        axios.get("http://localhost:5000/user/test")
            .then(res => {
                const gotApi = res.data;
                this.setState({
                    repos: gotApi
                })
            })
    }

    render() {
        return (
            <React.Fragment>
                <table>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Gender</td>
                            <td>Name</td>
                            <td>Surname</td>
                            <td>Date of Birth</td>
                            <td>Email</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.repos.map(repo => (
                            <tr key={repo._id}>
                                <td>
                                    {repo._id}
                                </td>
                                <td>
                                    {repo.sex}
                                </td>
                                <td>
                                    {repo.name}
                                </td>
                                <td>
                                    {repo.surname}
                                </td>
                                <td>
                                    {repo.dob}
                                </td>
                                <td>
                                    {repo.email}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
export default Registration